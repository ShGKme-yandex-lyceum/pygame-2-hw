# Знакомство с PyGame и git

## Домашнее задание

В черном окне размером 400*300 пикселей по нажатию кнопки мыши появляется круг радиусом 10 пикселей случайного цвета.  
Этот круг начинает падать вниз со скоростью 100 пикселей в секунду. Упав вниз, круг остается лежать на месте, пока падают другие круги.

## Contributing

Check https://gitmoji.carloscuesta.me and https://github.com/RomuloOliveira/commit-messages-guide for commit messages convention.

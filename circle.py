class Circle:
    def __init__(self, position, radius=1, color=(0, 0, 0)):
        self._position = position
        self._radius = radius
        self._color = color

    def position(self):
        return self._position

    def set_position(self, x, y):
        self._position = (x, y)

    def radius(self):
        return self._radius

    def color(self):
        return self._color

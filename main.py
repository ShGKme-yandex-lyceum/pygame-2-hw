import pygame

from circle import Circle
from utils import random_color


class FallingBallsGame:
    def __init__(self):
        self.SCREEN_SIZE = 400, 300
        self.CIRCLE_RADIUS = 10
        self.GAME_CAPTION = 'Falling Balls'
        self.DISPLAY_COLOR = (0, 0, 0, )
        self.BAKED_COLOR = (255, 255, 255,)
        self.FALLING_SPEED = 100
        self.circles = []
        self.screen = None
        self.baked_screen = None
        self.tics = 0
        self._init_pygame()

    def _init_pygame(self):
        pygame.init()
        pygame.display.set_caption(self.GAME_CAPTION)
        self.screen = pygame.display.set_mode(self.SCREEN_SIZE)
        self.baked_screen = pygame.Surface(self.screen.get_size())
        self.baked_screen.fill(self.BAKED_COLOR)
        self.clock = pygame.time.Clock()

    def start(self):
        is_running = True
        while is_running:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    is_running = False
                if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
                    self._add_circle(event.pos)
            self.tics = self.clock.tick()
            self._physics()
            self._render()
        pygame.quit()

    def _physics(self):
        for circle in self.circles:
            self._update_circle_position(circle)

    def _render(self):
        self.screen.fill(self.DISPLAY_COLOR)
        self.screen.blit(self.baked_screen, (0, 0))
        self._draw_circles()
        pygame.display.flip()

    def _add_circle(self, position):
        circle = Circle(position, self.CIRCLE_RADIUS, random_color())
        self.circles.append(circle)

    def _draw_circles(self):
        for circle in self.circles:
            pygame.draw.circle(self.screen, circle.color(), tuple(map(int, circle.position())), circle.radius())

    def _update_circle_position(self, circle):
        seconds = self.tics / 1000
        if circle.position()[1] >= self.SCREEN_SIZE[1] - circle.radius():
            return
        circle.set_position(circle.position()[0], circle.position()[1] + seconds * self.FALLING_SPEED)


balls_game = FallingBallsGame()
balls_game.start()
